package edclibrary;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.Stack;
 
public class HexaConverter {
        public static byte[] asciiToEDC(String asciiString, String type, int width){
                byte[] hexa=new byte[0];
                if(type.equalsIgnoreCase("numeric")){
                    	asciiString=asciiString.toUpperCase();
                        if(asciiString.length()%2!=0){
                                asciiString="0"+asciiString;   
                        }
                        for(int i=0;i<asciiString.length();i+=8){
                        	 int hexaInt;
                             int total=0;
                             int j;
                             for(j=i;j<asciiString.length()&&j<(i+8);j+=2){
                                     char first=asciiString.charAt(j);
                                     char last=asciiString.charAt(j+1);
                                     //System.out.println("To be convert:"+first+last);
                                     int firstInt,lastInt;
                                     if(first>=65 && first<=90)
                                             firstInt=first-'7';
                                     else firstInt=first-'0';
                                     if(last>=65 && last<=90)
                                             lastInt=last-'7';
                                     else lastInt=last-'0';
                                     firstInt<<=4;
                                     hexaInt=firstInt+lastInt;
                                     if(i<asciiString.length())total<<=8;
                                     total+=hexaInt;
                             }
                             //System.out.println(total);
                             byte[] resultInt=toBytes(total,(j+1-i)/2);
                             byte[] temp=hexa;
                             //System.out.println("header:"+temp.length+"footer:"+resultInt.length);
                             hexa=new byte[hexa.length+resultInt.length];
                             System.arraycopy(temp, 0, hexa, 0, temp.length);
                             System.arraycopy(resultInt, 0, hexa, temp.length, resultInt.length);
                        }                        
                }
                else{
                        hexa=asciiString.getBytes();
                }
                
                //Change hexa size to width
                int gap=width-hexa.length;
    			byte[] addition=new byte[gap];
        		byte[] temp;
                if(gap>0){
            		int newAllocate=hexa.length+gap;
                	if(type.equalsIgnoreCase("numeric")){
                		for(int i=0;i<gap;i++){
                			addition[i]=(byte)0xFF;
                		}
                		temp=hexa;
                		hexa=new byte[newAllocate];
                		System.arraycopy(temp, 0, hexa, 0, temp.length);
                		System.arraycopy(addition, 0, hexa, temp.length, gap);
                	}else{
                		for(int i=0;i<gap;i++){
                			addition[i]=0x20;
                		}
                		temp=hexa;
                		hexa=new byte[newAllocate];
                		System.arraycopy(temp, 0, hexa, 0, temp.length);
                		System.arraycopy(addition, 0, hexa, temp.length, gap);
                	}
                }
                return hexa;
        }
        
        public static String setByteTo(char c, int w){
        	String result;
        	try{
        		byte[] barray=new byte[w];
            	for(int i=0;i<w;i++){
            		barray[i]=(byte)c;
            	}
            	result=new String(barray);
        	}catch(Exception e){
        		result=null;
        	}
        	return result;
        }
        
        public static byte[] toBytes(int number, int bytes)
        {
          byte[] result = new byte[bytes];
          for(int i=bytes;i>0;i--){
                  result[bytes-i] = (byte) (number >> 8*(i-1));
          }
 
          return result;
        }
       
    	private static String incrementFileName(String fileName){
        	String seq=fileName.substring(fileName.indexOf(".ITU")+4,fileName.length());
        	int number=Integer.parseInt(seq);
        	number++;
        	fileName=fileName.substring(0, fileName.indexOf("ITU")+3)+number;
        	return fileName;
    	}
    	
        public static void main(String args[]){

        }
       
        public static void printToBinaryFile(String fileName,byte[] b){
                try{                       
                        FileOutputStream outputStream =new FileOutputStream(fileName);
                        outputStream.write(b);
                        outputStream.close();
                }catch(IOException e){
                        e.printStackTrace();
                }
        }
        
        public static void printToBinaryFileWithoutLossingData(String fileName,byte[] b){
            try{           
                FileOutputStream outputStream =new FileOutputStream(fileName,true);
                outputStream.write(b);
                outputStream.close();
	        }catch(IOException e){
	                e.printStackTrace();
	        }
        }
}