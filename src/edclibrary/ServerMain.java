package edclibrary;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.swing.plaf.FileChooserUI;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.codehaus.jackson.node.ObjectNode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
/*KONVENSI PENAMAAN
 * Nama file.hbm.xml mengikuti nama kelas tanpa .
 * Nama kelas mengikuti nama tabel fisik tanpa <spasi> dan tanpa .
 * Nama kolom java LOWERCASE
 * Nama kolom db UPPERCASE
 */
public class ServerMain {

	//Mapping definition
	static final Map<Character, String> typeMapping;
	private static Map<String,ArrayList<Schema>> mapAfterConverted;
	
	static {
	  Map<Character, String> tmp = new HashMap<>();
	  tmp.put('A', "string");
	  tmp.put('B', "byte");
	  tmp.put('I', "string"); //string(255)
	  tmp.put('L', "character"); //character(1)
	  tmp.put('N', "string");
	  tmp.put('P', "string"); //character(255)
	  tmp.put('T', "string");
	  typeMapping = Collections.unmodifiableMap(tmp);
	}
	
	private static void deleteAllFiles(String path){
		File dir=new File(path);
		for(File file: dir.listFiles()) file.delete();
	}
	
	public static void main(String[] args){
		Scanner scan=new Scanner(System.in);
		while(true){
			try{
				Map<String, ArrayList<Schema>> allDEF;
				ArrayList<String>  model=new ArrayList<String>();
				
				//Read DEF definition
				SchemaReader reader=new SchemaReader();
				allDEF=reader.loadAllDEF();
				System.out.println("=====KEY=====");
				for(String key: allDEF.keySet()){
					System.out.println(key);
				}
				System.out.println("=============");
				if(allDEF==null){
					System.out.println("Something wrong with .DEF in currentDEF");
					break;
				}

				for ( String key : allDEF.keySet() ) {
				    model.add(key);
				}
				
				//Setup configuration file
				for (String key : allDEF.keySet()){
					createXMLMapping(key, allDEF.get(key));
				}
				modifHibernateCfg(model);
				
				System.out.println("\nConfigure finished");
				SessionFactory sessionFactory=null;
				Session session=null;
				try{
					sessionFactory=createSessionFactory();
					session=sessionFactory.openSession();
					//Start thread file comparator
					FileComparator comparator=new FileComparator("../Kartuku/CurrentDEF","../Kartuku/ImportingDEF");
					comparator.start();

					Queri terminal;
					while(!comparator.restart){ //Serve the request
						String id=scan.nextLine();
						List<HashMap> l=null;
						try{
							session.beginTransaction();
							deleteAllFiles("../Kartuku/Output");//clean up all files in output folder
							terminal=new Queri("terminal","tid",id);
							ObjectNode temp=terminal.executeQueri(session, allDEF,SchemaReader.getTabPK());
							Queri.getHashMap().clear();
							System.out.println("Hasil final:"+temp);
						}catch(Exception e){
							System.out.println(e.getMessage());
						}
						session.getTransaction().commit();
						//System.out.println("Query Result : "+l);	
					}
					DeleteAllHbmXML();
				}catch(Exception e){
					System.out.println(e.getMessage());
				}
				if(session!=null)session.close();
				if(sessionFactory!=null)sessionFactory.close();
			}catch (Exception e) {
				System.out.println(e.getMessage());
				break;
			}
		}
	}
	
	private static void DeleteAllHbmXML(){
		File fileSystem=new File("../Kartuku/src/");
		File[] files=fileSystem.listFiles();
		for(int i=0;i<files.length;i++){
			if(files[i].isFile()){
				try{
					String filename=files[i].getName();	
					String extension=filename.substring(filename.lastIndexOf(".hbm.xml"),filename.length());
					if(extension.equalsIgnoreCase(".hbm.xml")){
						files[i].delete();
					}
				}catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		}
		
	}
	private static SessionFactory createSessionFactory(){
		SessionFactory sessionFactory;
		ServiceRegistry serviceRegistry;
		Configuration configuration=new Configuration();
		configuration.configure();
		serviceRegistry =new StandardServiceRegistryBuilder().applySettings(
	            configuration.getProperties()).build();
		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	    return sessionFactory;
	}
	
	private static void createXMLMapping(String modelName,ArrayList<Schema> colDefined){
		try{
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc=docBuilder.newDocument();

			Transformer transformer=TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			
			DOMImplementation domImpl=doc.getImplementation();
			DocumentType doctype=domImpl.createDocumentType("doctype", "-//Hibernate/Hibernate Mapping DTD//EN", 
					"http://www.hibernate.org/dtd/hibernate-mapping-3.0.dtd");
			
			transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, doctype.getPublicId());
			transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, doctype.getSystemId());
			
			//DEFINED ALL ELEMENT
			Element hibernateMapping = doc.createElement("hibernate-mapping");
			doc.appendChild(hibernateMapping);
	 
			Element clazz = doc.createElement("class");
			clazz.setAttribute("table",'`'+modelName+'`');
			clazz.setAttribute("entity-name",modelName.replaceAll("[^a-zA-Z]", ""));
			hibernateMapping.appendChild(clazz);
		
			Element elementID=doc.createElement("id");
			elementID.setAttribute("name", "id");
			elementID.setAttribute("column", "ID");
			elementID.setAttribute("type", "long");
			
			Element elementGenerator=doc.createElement("generator");
			elementGenerator.setAttribute("class", "assigned");
			elementID.appendChild(elementGenerator);
			
			clazz.appendChild(elementID);

			for(int i=0;i<colDefined.size();i++){
				//Map only visible column
				Element property=doc.createElement("property");
				property.setAttribute("name", colDefined.get(i).getName().toLowerCase().replaceAll("\\s", ""));
				property.setAttribute("column", '`'+colDefined.get(i).getName().toUpperCase()+'`');
				property.setAttribute("type", typeMapping.get(colDefined.get(i).getType()));
				clazz.appendChild(property);
			}
			
			DOMSource source=new DOMSource(doc);
			StreamResult result=new StreamResult(new File("../Kartuku/src/"+modelName+".hbm.xml"));
			transformer.transform(source, result);
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	//Renew model identified
	private static void modifHibernateCfg(ArrayList<String> model){
		   try{
		        String filepath = "../Kartuku/src/hibernate.cfg.xml";
		        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		        System.out.println("START");
		        Document doc = docBuilder.parse(filepath);
		        //Mengambil elemen berdasarkan tag
		        Node sessionFactory = doc.getElementsByTagName("session-factory").item(0);
		        NodeList existMapping=doc.getElementsByTagName("mapping");
		        for(int i=existMapping.getLength()-1;i>=0;i--){
		        	sessionFactory.removeChild(existMapping.item(i));
		        }
		        for(int j=0;j<model.size();j++){
		        	Element elementMapping=doc.createElement("mapping");
		        	elementMapping.setAttribute("resource",model.get(j)+".hbm.xml");
		        	sessionFactory.appendChild(elementMapping);
		        }
			  DOMImplementation domImpl=doc.getImplementation();
			  DocumentType doctype=domImpl.createDocumentType("doctype",  "-//Hibernate/Hibernate Configuration DTD//EN", 
					  "http://www.hibernate.org/dtd/hibernate-configuration-3.0.dtd");
		      //Menulis data ke file xml
		      Transformer transformer = TransformerFactory.newInstance().newTransformer();
			  transformer.setOutputProperty(OutputKeys.INDENT, "no");

			  transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, doctype.getPublicId());
			  transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, doctype.getSystemId());
			  
		      DOMSource source = new DOMSource(doc);
		      StreamResult result =  new StreamResult(new File("../Kartuku/src/hibernate.cfg.xml"));
		      transformer.transform(source, result);
		      System.out.println("FINISH");
		      Thread.sleep(3000);
		     }catch(ParserConfigurationException pce){
		       System.out.print(pce.getMessage());
		       System.out.println("1");
		     }catch(TransformerException tfe){
		       System.out.print(tfe.getMessage());
		       System.out.println("2");
		     }catch(IOException ioe){
		       System.out.print(ioe.getMessage());
		       System.out.println("3");
		     }catch(SAXException sae){
		       System.out.print(sae.getMessage());
		       System.out.println("4");
		     }catch(Exception e){
		    	e.printStackTrace();
		    	System.out.println("5");
		    }
	}
}






















