package edclibrary;

public class TableRelation {
	private String colRefer;
	private String tableReferred;
	private String colReferred;
	
	public TableRelation(String colRefer,String tableReferred, String colReferred){
		this.colRefer=colRefer;
		this.tableReferred=tableReferred;
		this.colRefer=colReferred;
	}
	
	public String getColRefer(){
		return colRefer;
	}
	public String getTableReferred(){
		return tableReferred;
	}
	public String getColRefereed(){
		return colReferred;
	}
}
