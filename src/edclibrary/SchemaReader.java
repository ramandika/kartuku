package edclibrary;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sound.midi.SysexMessage;

import ru.smartflex.tools.dbf.DbfEngine;
import ru.smartflex.tools.dbf.DbfHeader;
import ru.smartflex.tools.dbf.DbfIterator;
import ru.smartflex.tools.dbf.DbfRecord;


public class SchemaReader {
	//List of Schema name and path
	private static String PATH="../Kartuku/CurrentDEF";
	private static Map<String,String> tableNameToDBF=null;
	private static Map<String,String> tabPK;
	
	
	public static Map<String,String> getTabPK(){
		return tabPK;
	}
	
	public static String tableNameToDBF(String tableName){
		return tableNameToDBF.get(tableName);
	}
	
	public String getPrimaryKey(String tableName){
		String key=null;
		
		return key;
	}
	
	//Load All .DEF files
	public Map<String,ArrayList<Schema>> loadAllDEF(){
		Map<String,ArrayList<Schema>> allDef=new HashMap<>();
		tabPK=new HashMap<>();
		tableNameToDBF=readCONTROL(PATH);
		if(tableNameToDBF!=null){
			File fileSystem=new File(PATH);
			File[] allFiles=fileSystem.listFiles();
			for(int i=0;i<allFiles.length;i++){
				String fileName=allFiles[i].getName();
				String format=fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
				if(allFiles[i].isFile() && format.equals("DEF") && !fileName.equalsIgnoreCase("CONTROL.DEF")){
					allDef.put(fileName.substring(0, fileName.lastIndexOf('.')).toLowerCase(), readSchema(PATH,fileName));
				}
			}	
		}else allDef=null;
        return allDef;
	}
	
	//Read File CONTROL.DEF
	private Map<String, String> readCONTROL(String path){
		Map<String,String> defToTableName=new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);
		try{
			 DbfHeader dbfHeader = DbfEngine.getHeader(path,
	                 "CONTROL.DEF", null);
	       	 DbfIterator dbfIterator = dbfHeader.getDbfIterator();
	    	 //Start to read 
	         while (dbfIterator.hasMoreRecords()) 
	       	 {
	               DbfRecord dbfRecord = dbfIterator.nextRecord();
	               String tableName = dbfRecord.getString("NAME");
	               String dbfName=dbfRecord.getString("DBFNAME"); dbfName=dbfName.substring(0, dbfName.lastIndexOf('.'));
	               defToTableName.put(tableName,dbfName.toLowerCase());
	         }
		}catch(Exception e){
			e.printStackTrace();
			defToTableName=null;
		}
		return defToTableName;
		
	}
	//readData from .DBF file and save to ArrayList<Schema>
    public ArrayList<Schema> readSchema(String path, String filename){
		ArrayList<Schema> schema=new ArrayList<>();
		Map<String, String> temp=new HashMap<>();
    	try{
			 DbfHeader dbfHeader = DbfEngine.getHeader(path,
	                 filename, null);
	       	 DbfIterator dbfIterator = dbfHeader.getDbfIterator();
	    	 
	    	 //Start to read
	       	 ArrayList<TableRelation> tableRel=new ArrayList<>();
	         while (dbfIterator.hasMoreRecords()) 
	       	 {
	               DbfRecord dbfRecord = dbfIterator.nextRecord();
	               String name = dbfRecord.getString("NAME");
	               Character type = dbfRecord.getString("TYPE").charAt(0);
	               int width= dbfRecord.getInt("WIDTH");
	               String comment= dbfRecord.getString("COMMENT");
	               if(comment==null) comment="null";
	               comment=comment.trim();
	               if(width>0 && (!comment.equalsIgnoreCase("unused") || type=='L') ){
	            	   String special=dbfRecord.getString("SPECIAL");
		               Pattern p = Pattern.compile("index\\(1\\):label\\((.*)\\)");
		               if(special==null)special="null";
			       	   Matcher m = p.matcher(special);
			       	   while (m.find()) {
			       			try{
			       				Boolean isPK=m.matches();
			       				if(isPK) tabPK.put(filename.substring(0,filename.lastIndexOf('.')).toLowerCase(), name);
			       			}catch(Exception e){
			       				e.printStackTrace();
			       			}
			       	   }
		               Schema schemaObj=new Schema(name,type,width,special);
		               schema.add(schemaObj);
	               }
	         }
    	}catch(Exception e){
    		e.printStackTrace();
    		schema=null;
    	}
    	return schema;
    }
}
