package edclibrary;

public class Schema {
	private String name,tableName,special;
	private int width;
	private Character type;
	
	public Schema(String name, Character type, int width,String special){
		this.name=name;
		this.type=type;
		this.width=width;
		this.special=special;
	}
	
	public String getSpecial(){
		return special;
	}	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Character getType() {
		return type;
	}
	public int getWidth() {
		return width;
	}
}
