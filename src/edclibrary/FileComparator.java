package edclibrary;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;



public class FileComparator extends Thread {
	private String importingFolder,usingFolder;
	public boolean restart;

	public FileComparator(String usingFolder,String importingFolder){
		this.usingFolder=usingFolder;
		this.importingFolder=importingFolder;
		restart=false;
	}
	
	private boolean isAnyFile(File filesystem){
		boolean existFile=true;
		if(filesystem.listFiles().length>0){
			existFile=true;
		}else{
			existFile=false;
		}
		return existFile;
		
	}
	
	public boolean checkContent(String filename,String filename1){
		boolean isSame;
		String oldContent="",newContent="",reader;
		BufferedReader br;
		try{
			br=new BufferedReader(new FileReader(filename));
			while((reader=br.readLine())!=null){oldContent+=reader;}
			br.close();
			oldContent=oldContent.replaceAll("\\s","");
			br=new BufferedReader(new FileReader(filename1));
			while((reader=br.readLine())!=null){newContent+=reader;}
			br.close();
			newContent=newContent.replaceAll("\\s", "");
			if(oldContent.equals(newContent)){
				isSame=true;
			}else{
				isSame=false;
			}
			
		}catch(Exception e){
			e.printStackTrace();
			isSame=false;
		}
		return isSame;
		
	}
	
	public void run(){
		try{
			File importFolder=new File(importingFolder);
			//Check Is Any new File in importingFolder
			while(!isAnyFile(importFolder)){
				Thread.sleep(1000);
			}

			//Comparing file
			File usedFolder=new File(usingFolder);
			File[] oldFile=usedFolder.listFiles();
			File[] newFile=importFolder.listFiles();
			
/*			for(int i=0;i<newFile.length;i++){
				String filename=newFile[i].getName();	
				String extension=filename.substring(filename.lastIndexOf(".") + 1, filename.length());
				boolean notExist=false;
				if(newFile[i].isFile() && extension.equalsIgnoreCase("DEF")){
					int counter=0;
					for(int j=0;j<oldFile.length;j++){
						String filename1=oldFile[j].getName();
						if(filename1.equalsIgnoreCase(filename)){
							//Check whether content different
							boolean isDifferent=checkContent(filename,filename1);
							if(isDifferent){
								//Move new .DEF to currentFolder
								try{
									newFile[j].renameTo(new File("../Kartuku/CurrentDEF/"+filename));
									newFile[j].delete();
								}catch(Exception e){
									e.printStackTrace();
								}
								break;
							}
						}
						if(counter==(oldFile.length-1)) notExist=true;
						counter++;
					}
					
					if(notExist){
						try{
							newFile[i].renameTo(new File("../Kartuku/CurrentDEF/"+filename));
							newFile[i].delete();
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
			}*/
			
			//Delete all old files
			for(int i=0;i<oldFile.length;i++) oldFile[i].delete();
			//Move all new files
			for(int i=0;i<newFile.length;i++){
				newFile[i].renameTo(new File("../Kartuku/CurrentDEF/"+newFile[i].getName()));
				newFile[i].delete();
			}
			
			//Reload Main Program
			restart=true;
			System.out.println("Restart program, loading new DEF");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
