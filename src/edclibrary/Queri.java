package edclibrary;

import java.io.File;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.hibernate.Session;

public class Queri {
	private String tableName;
	private String column;
	private String queriStatement;
	private String colVal;
    private static Pattern referenced = Pattern.compile("on\\((.*)\\):(get|list)?\\((.*)\\)");
    private static Pattern fill=Pattern.compile("fill\\((.*)\\)");
    private static Pattern pk = Pattern.compile("index\\(1\\):label\\((.*)\\)");
    private static Pattern match=Pattern.compile("map\\((.*)\\((.*),(.*),(.*)\\)\\)");
    private static Pattern set=Pattern.compile("set\\((.*)\\((.*)\\)\\)");
    private static Map<String,String> pkTable;
    private static ObjectMapper mapper;
    private static boolean sameFile=true;
    private static HashMap<String, List<HashMap>> hashMap=new HashMap<>();
	
    public static HashMap<String,List<HashMap>> getHashMap(){
    	return hashMap;
    }
    
	public String standardizeColumn(String columnName){
		return columnName.toLowerCase().replaceAll("\\s", "");
	}
	
	//Relation table to query result
	private class relTabToQueRes{
		private String tableName;
		private List<HashMap> QueryResult;
		
		public relTabToQueRes(String tableName,List<HashMap> l){
			this.tableName=tableName;
			this.QueryResult=l;
		}
		
		public String getTableName(){
			return tableName;
		}
		
		public List<HashMap> getList(){
			return QueryResult;
		}
	}
	
	public Queri(String tableName,String column, String colVal){
		this.tableName=tableName;
		this.column=column;
		this.colVal=colVal;
		if(tableName.equals("issuer")){
			this.queriStatement="from "+this.tableName.replaceAll("[^a-zA-Z0-9]", "")+" where "+
					this.column+" LIKE '"+colVal+"'";
		}else{
			this.queriStatement="from "+this.tableName.replaceAll("[^a-zA-Z0-9]", "")+" where "+
					this.column+"='"+colVal+"'";	
		}
		mapper=new ObjectMapper();
	}
	
	public String getQueriStatement() {
		return queriStatement;
	}
	
	private static String incrementFileName(String fileName){
    	String seq=fileName.substring(fileName.indexOf(".ITU")+4,fileName.length());
    	int number=Integer.parseInt(seq);
    	number++;
    	fileName=fileName.substring(0, fileName.indexOf("ITU")+3)+number;
    	return fileName;
	}
	
	private static void printStack(Stack s){
		Iterator<relTabToQueRes> it=s.iterator();
		boolean found=false;
		while(it.hasNext() && !found){
			relTabToQueRes temp=it.next();
			if(temp.getTableName().equals("keydata"))
			System.out.println("=["+temp.getTableName()+","+temp.getList()+"\t");
		}
	}
	
	private static String getHighestFileNameAvl(String fileName){
		String highestFileName=fileName;
		File f=new File(highestFileName);
		while(f.exists()){
			highestFileName=incrementFileName(highestFileName);
			//Max FilaName 3 digits
			String indexStr=highestFileName.substring(highestFileName.indexOf(".ITU")+4, highestFileName.length());
			for(int i=0;i<(4-indexStr.length());i++){
				indexStr='0'+indexStr;
			}
			highestFileName=highestFileName.substring(0, highestFileName.indexOf("ITU")+3)+indexStr;
			f=new File(highestFileName);
		}
		return highestFileName;
	}
	
	
	@SuppressWarnings("unchecked")
	public ObjectNode executeQueri(Session session, Map<String,ArrayList<Schema>> map,Map<String,String> pkTable){
	    byte[] partition=new byte[0];	
		String query=this.getQueriStatement();
		session.clear();
		//Get all column name with value
		List<HashMap> l=session.createQuery(query).list();
		
		hashMap.put(this.tableName, l); //Push query to hashmap
		
		JsonNode arrayNode=mapper.createArrayNode();
		JsonNode objNode=mapper.createObjectNode();
		try{
			for(int i=0;i<l.size();i++){
				HashMap colWithVal=l.get(i);
				//Iterate using predefined col in map
				ArrayList<Schema> schema=map.get(this.tableName);
				String colName,value,special,pkValue="";
				char type;
				int width;
				Matcher m;
				
				//Give name to table
				String fileName="../Kartuku/Output/"+this.tableName+".ITU001";
				File f=new File(fileName);
				if(f.exists()){
					fileName=getHighestFileNameAvl(fileName);
				}
				
				if(this.tableName.equals("terminal")||this.tableName.equals("acquirer")||this.tableName.equals("issuer")||this.tableName.equals("card")){
					//Get primary key value
					for(int j=0;j<schema.size();j++){
						Schema attribute=schema.get(j);
						try{
							special=attribute.getSpecial();
							m=pk.matcher(special);
							while(m.find()){
								Boolean isPK=m.matches();
								if(isPK){
									colName=standardizeColumn(attribute.getName());
									pkValue=colWithVal.get(colName).toString();
									((ObjectNode)objNode).put(this.tableName, pkValue);
									break;
								}
							}
						}catch(Exception e){e.printStackTrace();}
					}
					//Get All Fields
					byte[] valCol = null;
					int counterLogic=0;
					byte logicalVal=0;
					for(int j=0;j<schema.size();j++){
						value=null;
						Schema attribute=schema.get(j);
						try{
							colName=standardizeColumn(attribute.getName());
							type=attribute.getType();
							width=attribute.getWidth();
							special=attribute.getSpecial();
							Object objValue=colWithVal.get(colName);
							Boolean nullobjVal;
							
							//if(colName.equalsIgnoreCase("tblnmbr") || colName.equalsIgnoreCase("duptblnmbr") || colName.equalsIgnoreCase("tableid") || colName.equalsIgnoreCase("expnum") )
							if(special.equals("set(position(1,))"))
							{
								int currentTblNmbr=Integer.parseInt(fileName.substring(fileName.indexOf(".ITU")+4,fileName.length()));
								value=Integer.toString(currentTblNmbr);
							}
							if(objValue==null && value==null){
								nullobjVal=true;
								//Cek match on special
								m=match.matcher(special);
								while(m.find()){
									String tableName=m.group(1);
									String colPointed=m.group(2);
									String colToGet=m.group(3);
									String colInThisTable=m.group(4);
									colInThisTable=standardizeColumn(colInThisTable);
									colPointed=standardizeColumn(colPointed);
									String v4lue=colWithVal.get(colInThisTable).toString();
									session.clear();
									Queri q=new Queri(SchemaReader.tableNameToDBF(tableName), colPointed, v4lue);
									List<HashMap> resultQuery=session.createQuery(q.getQueriStatement()).list();
									HashMap resultMap=resultQuery.get(0);
									value=resultMap.get(standardizeColumn(colToGet)).toString();
								}
								m=set.matcher(special);
								while(m.find()){
									String tableName=m.group(1);
									String colToGet=m.group(2);
									List<HashMap> temp=hashMap.get(tableName.toLowerCase());
									if(temp!=null){
										HashMap hm = temp.get(0);
										if(hm!=null){
											Object obj=hm.get(standardizeColumn(colToGet));
											if(obj!=null){
												value= obj.toString();	
											}	
										}	
									}
									
								}
								if(value==null){
									//Check for fill
									m=fill.matcher(special);
									while(m.find()){
										try{
											value=m.group(1);
											int filInt=Integer.parseInt(value);
											char filChar=(char)filInt;
											value=HexaConverter.setByteTo(filChar,width);
										}catch(Exception e){
											e.printStackTrace();
										}
									}
									if(value==null){
										value="";
										if(type=='A'){
											value+=' ';
										}else{
											value+="FF";
										}	
									}	
								}
							}else{
								nullobjVal=false;
								if(value==null)value=objValue.toString();
								m=set.matcher(special);
								while(m.find()){
									String tableName=m.group(1);
									String colToGet=m.group(2);
									System.out.println(hashMap);
									System.out.println(tableName);
									List<HashMap> temp=hashMap.get(tableName.toLowerCase());
									if(temp!=null){
										HashMap hm = temp.get(0);
										if(hm!=null){
											Object obj=hm.get(standardizeColumn(colToGet));
											if(obj!=null){
												value= obj.toString();	
											}	
										}	
									}
									
								}
								((ObjectNode)objNode).put(colName, value);
							}
							if(!nullobjVal){
								/*==============================GET SPECIAL TO KNOW RELATED TABLE===========================*/
								m = referenced.matcher(special);
								while (m.find()) {
									try{
										String referenced=m.group(3); // print entire matched substring
										referenced=referenced.replaceAll("\"", "");
										String v4lue;
										referenced=SchemaReader.tableNameToDBF(referenced);
										if(referenced.equals("acquirer")){
											v4lue=pkValue+value;
										}
										else if(referenced.equals("issuer")){
											v4lue=pkValue+'%'+value;
										}
										else {
											v4lue=value;
										}
										String column=pkTable.get(referenced);
										if(column!=null){
											Queri q=new Queri(referenced, column.toLowerCase(), v4lue);
											JsonNode temp=q.executeQueri(session, map, pkTable);
											if(temp.size()>0) ((ArrayNode)arrayNode).add(temp);	
										}
									}catch(Exception e){
										e.printStackTrace();
									}
								}
								/*==========================================================================================*/	
							}
							/*===============================WRITE TO BINARY FILE*======================================*/
							//Manipulate value from column
							if(type!='I' && type!='P'){ //Type I & P not written
								if(type=='L'){
									if(counterLogic<7){
										if(value.equalsIgnoreCase("Y") || value.equalsIgnoreCase("1"))
											logicalVal+=(byte)1<<counterLogic;
										counterLogic++;
									}else{
										if(value.equalsIgnoreCase("Y") || value.equalsIgnoreCase("1")){
											logicalVal+=((byte)1<<counterLogic);
										}
										valCol=ByteBuffer.allocate(1).put(logicalVal).array();
										byte[] temp=partition;
										partition=new byte[valCol.length+partition.length];
										System.arraycopy(temp, 0, partition, 0, temp.length);
										System.arraycopy(valCol, 0, partition, temp.length, valCol.length);
										counterLogic=0;
										logicalVal=0;
									}
								}else{
									if(type=='N'){
										//Cek width
										valCol=HexaConverter.asciiToEDC(value, "numeric",width);
										byte[] temp=partition;
										partition=new byte[valCol.length+partition.length];
										System.arraycopy(temp, 0, partition, 0, temp.length);
										System.arraycopy(valCol, 0, partition, temp.length, valCol.length);
									}else if (type=='A'){
										//Cek width
										valCol=HexaConverter.asciiToEDC(value, "non-numeric",width);
										byte[] temp=partition;
										partition=new byte[valCol.length+partition.length];
										System.arraycopy(temp, 0, partition, 0, temp.length);
										System.arraycopy(valCol, 0, partition, temp.length, valCol.length);
									}else{
										System.out.println(type+" UNDEFINED on"+this.tableName+" coloumn"+value);
									}
								}
							}
							if((partition.length>1024 && counterLogic==0) || j==(schema.size()-1)){
								if(sameFile)HexaConverter.printToBinaryFileWithoutLossingData(fileName, partition);
								else{
									HexaConverter.printToBinaryFile(fileName, partition);
								}
								partition=new byte[0];
																			
							}
							/*===========================================================================================*/
						}catch(Exception e){e.printStackTrace();}
					}
				}else{
					//Get All fields
					byte[] valCol = null;
					int counterLogic=0;
					byte logicalVal=0;
					for(int j=0;j<schema.size();j++){
						value=null;
						Schema attribute=schema.get(j);
						try{
							colName=standardizeColumn(attribute.getName());
							Object objValue=colWithVal.get(colName);
							type=attribute.getType();
							special=attribute.getSpecial();
							width=attribute.getWidth();
							boolean nullObjVal;
							//if(colName.equalsIgnoreCase("tblnmbr") || colName.equalsIgnoreCase("duptblnmbr") || colName.equalsIgnoreCase("tableid") || colName.equalsIgnoreCase("expnum"))
							if(special.equals("set(position(1,))"))
							{
								int currentTblNmbr=Integer.parseInt(fileName.substring(fileName.indexOf(".ITU")+4,fileName.length()));
								value=Integer.toString(currentTblNmbr);
							}
							if(objValue==null && value==null){
								nullObjVal=true;
								//Check matcher
								m=match.matcher(special);
								while(m.find()){
									String tableName=m.group(1);
									String colPointed=m.group(2);
									String colToGet=m.group(3);
									String colInThisTable=m.group(4);
									colInThisTable=standardizeColumn(colInThisTable);
									colPointed=standardizeColumn(colPointed);
									String v4lue=colWithVal.get(colInThisTable).toString();
									session.clear();
									Queri q=new Queri(SchemaReader.tableNameToDBF(tableName), colPointed, v4lue);
									List<HashMap> resultQuery=session.createQuery(q.getQueriStatement()).list();
									HashMap resultMap=resultQuery.get(0);
									value=resultMap.get(standardizeColumn(colToGet)).toString();
								}
								m=set.matcher(special);
								while(m.find()){
									String tableName=m.group(1);
									String colToGet=m.group(2);
									List<HashMap> temp=hashMap.get(tableName.toLowerCase());
									if(temp!=null){
										HashMap hm = temp.get(0);
										if(hm!=null){
											Object obj=hm.get(standardizeColumn(colToGet));
											if(obj!=null){
												value= obj.toString();	
											}	
										}
									}
									
								}
								if(value==null){
									//Check for fill
									m=fill.matcher(special);
									while(m.find()){
										try{
											value=m.group(1);
											int filInt=Integer.parseInt(value);
											char filChar=(char)filInt;
											value=HexaConverter.setByteTo(filChar,width);
										}catch(Exception e){
											e.printStackTrace();
										}
									}
									if(value==null){
										value="";
										if(type=='A'){
											value+=' ';
										}else{
											value+="FF";
										}	
									}	
								}
							}else{
								nullObjVal=false;
								if(value==null)value=objValue.toString();
								m=set.matcher(special);
								while(m.find()){
									String tableName=m.group(1);
									String colToGet=m.group(2);
									List<HashMap> temp=hashMap.get(tableName.toLowerCase());
									if(temp!=null){
										HashMap hm = temp.get(0);
										if(hm!=null){
											Object obj=hm.get(standardizeColumn(colToGet));
											if(obj!=null){
												value= obj.toString();	
											}	
										}
									}
								}
								((ObjectNode)objNode).put(colName, value);
							}
							m=pk.matcher(special);
							if(m.matches()){
								((ObjectNode)objNode).put(this.tableName,value);	
							}
							if(!nullObjVal){
								m = referenced.matcher(special);
								while (m.find()) {
									try{
										String referenced=m.group(3); // print entire matched substring
										referenced=referenced.replaceAll("\"", "");
										referenced=SchemaReader.tableNameToDBF(referenced);
										String column=pkTable.get(referenced);
										if(column!=null){
											Queri q=new Queri(referenced, pkTable.get(referenced).toLowerCase(), value);
											JsonNode temp=q.executeQueri(session, map, pkTable);
											if(temp.size()>0)((ArrayNode)arrayNode).add(temp);	
										}
										
									}catch(Exception e){
										e.printStackTrace();
									}
								}	
							}
							//Manipulate value from column
							if(type!='I' && type!='P'){ //Type I & P not written
								if(type=='L'){
									if(counterLogic<7){
										if(value.equalsIgnoreCase("Y") || value.equalsIgnoreCase("1"))
											logicalVal+=(byte)1<<counterLogic;
										counterLogic++;
									}else{
										if(value.equalsIgnoreCase("Y") || value.equalsIgnoreCase("1")){
											logicalVal+=((byte)1<<counterLogic);
										}
										valCol=ByteBuffer.allocate(1).put(logicalVal).array();
										byte[] temp=partition;
										partition=new byte[valCol.length+partition.length];
										System.arraycopy(temp, 0, partition, 0, temp.length);
										System.arraycopy(valCol, 0, partition, temp.length, valCol.length);
										counterLogic=0;
										logicalVal=0;
									}
								}else{
									if(type=='N'){
										//Cek width
										valCol=HexaConverter.asciiToEDC(value, "numeric",width);
										byte[] temp=partition;
										partition=new byte[valCol.length+partition.length];
										System.arraycopy(temp, 0, partition, 0, temp.length);
										System.arraycopy(valCol, 0, partition, temp.length, valCol.length);
									}else if (type=='A'){
										//Cek width
										valCol=HexaConverter.asciiToEDC(value, "non-numeric",width);
										byte[] temp=partition;
										partition=new byte[valCol.length+partition.length];
										System.arraycopy(temp, 0, partition, 0, temp.length);
										System.arraycopy(valCol, 0, partition, temp.length, valCol.length);
									}else{
										System.out.println(type+" UNDEFINED on "+this.tableName+" coloumn "+colName+ " value "+value);
									}
								}
						   }
							if((partition.length>1024 && counterLogic==0) || j==(schema.size()-1)){
								if(sameFile)HexaConverter.printToBinaryFileWithoutLossingData(fileName, partition);
								else{
									HexaConverter.printToBinaryFile(fileName, partition);
								}
								partition=new byte[0];
																			
							}
						}catch(Exception e){e.printStackTrace();}
					}
				}
				sameFile=false;
			}
			if(arrayNode.size()>0)((ObjectNode)objNode).put("has", arrayNode);
		}catch(Exception e){
			e.printStackTrace();
		}
		return (ObjectNode) objNode;
	}
}
